class Time {
	#m_DeltaTime: number;
	#m_FrameStartTime: number;
	#m_FrameEndTime: number;

	frameStart() {
		this.#m_FrameStartTime = Date.now();
	}

	frameEnd() {
		this.#m_FrameEndTime = Date.now();	
	}

	getDelta(): number {
		return this.#m_DeltaTime = (this.#m_FrameStartTime - this.#m_FrameEndTime) / 1000;
	}

	getTime(): number {
		return Date.now();
	}
}

const time = new Time();

export {time};