import { time } from "./Time.js";
import { money } from "./Money.js";
import Ride from "./Ride.js";

const startWindow: HTMLElement = document.getElementById("start-game-window");
const endWindow: HTMLElement = document.getElementById("end-game-window");
const startGameButton: HTMLElement = document.getElementById("start-game");
const game: HTMLElement = document.getElementById("game");

const totalSalesEl: HTMLElement = document.getElementById("total-sales");
var totalSales: number = 0;

money.setMoney(10000);

const lengthOfTime = 5000;
let startTime: number;

console.log(Number.MAX_VALUE);

startGameButton.addEventListener("mousedown", e => {
	if (e.button != 0)
		return;

	loadGame();

	startWindow.classList.replace("show", "hide");
	startTime = time.getTime();

	game.classList.replace("hide", "show");
});

const loadGame = () => {
	let rides: Ride[] = [];

	rides.push(new Ride("the-towers", "The Towers & Gardens", 15, 500, 40, 200, 0.04, 50, 2000, 5));

	rides.forEach(ride => {
		game.appendChild(ride.createElement());
	});

	let nextDay = time.getTime() + lengthOfTime;

	// Main Game Loop
	setInterval(() => {
		time.frameStart();

		if (money.getMoney() < -1000) {
			game.classList.replace("show", "hide");
			endWindow.classList.replace("hide", "show");
			return;
		}

		rides.forEach(ride => {
			ride.updateElemets();

			if (ride.hasBought) {
				let isbroken = Math.floor(Math.random() * 100 / ride.rideDowntimeMultiplier);

				if (isbroken <= 0)
					ride.isBroken = true;
			}


		});

		// Revenue

		totalSalesEl.textContent = totalSales.toString();

		if (nextDay < time.getTime()) {
			rides.forEach(ride => {
				if (ride.hasBought && !ride.isBroken)
					money.setMoney(-ride.rideRunningCost);
			});

			nextDay = time.getTime() + lengthOfTime;
		}

		time.frameEnd();
	}, (1000 / 60));
}
