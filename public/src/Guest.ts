import Ride from "./Ride.js"

class Guest {
	currentRide: Ride;
	satisfaction: number = 50;
}

export default Guest;
