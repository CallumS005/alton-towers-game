class Money {
	#money: number = 0;

	getMoney(): number {
		return this.#money;
	}

	setMoney(num: number): void {
		this.#money += num;

		const totalRevenueEl: HTMLElement = document.getElementById("total-revenue");
		totalRevenueEl.textContent = money.getMoney().toString();
	}
}

const money: Money = new Money();

export { money };