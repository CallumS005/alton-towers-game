import { money } from "./Money.js";
class Ride {
    id = "";
    name = "";
    rideCost = 0;
    rideSales = 0;
    rideTicketCost = 0;
    rideRunningCost = 0;
    rideMaintianceCost = 0;
    rideDowntimeMultiplier = 0;
    rideMaxCapacity = 0;
    rideCapacity = 0;
    rideLength = 0;
    rideAvgQueueTime = 0;
    rideQueue = 0;
    rideSatisfaction = 0;
    hasBought = false;
    isBroken = false;
    ridePanel;
    // Unlocks
    u_AutoTicketCost = Math.floor(0.054 * this.rideSales);
    u_AutoTicketQuantity = 0;
    u_AutoTicketRunningCost = 2;
    u_AutoTicketPerformance = 400;
    // Unlock Requirements
    #ur_AutoTicket = 10;
    constructor(id, name, ticketCost, rideCost, rideRunningCost, rideMaintinanceCost, rideDowntimeMul, rideCap, rideLen, rideSatisfaction) {
        this.id = id;
        this.name = name;
        this.rideTicketCost = ticketCost;
        this.rideCost = rideCost;
        this.rideRunningCost = rideRunningCost;
        this.rideMaintianceCost = rideMaintinanceCost;
        this.rideDowntimeMultiplier = rideDowntimeMul;
        this.rideMaxCapacity = rideCap;
        this.rideLength = rideLen;
        this.rideSatisfaction = rideSatisfaction;
        this.clearQueue();
        this.unlockAutoTicket();
    }
    sellTicket() {
        if (this.isBroken)
            return;
        this.rideSales++;
        money.setMoney(this.rideTicketCost);
        if (this.rideCapacity >= this.rideMaxCapacity)
            this.rideQueue++;
        else
            this.rideCapacity++;
        this.u_AutoTicketCost = Math.floor(0.054 * this.rideSales);
    }
    clearQueue() {
        setInterval(() => {
            if (!this.isBroken) {
                if (this.rideQueue > 0) {
                    this.rideQueue--;
                }
                else if (this.rideCapacity > 0) {
                    this.rideCapacity--;
                }
            }
        }, this.rideLength);
    }
    unlockAutoTicket() {
        setInterval(() => {
            for (let i = 0; i < this.u_AutoTicketQuantity; i++) {
                let noTickets = Math.floor(Math.random() * 5);
                for (let j = 0; j < noTickets; j++) {
                    this.sellTicket();
                }
            }
        }, this.u_AutoTicketPerformance);
    }
    createElement() {
        this.ridePanel = document.createElement("div");
        this.ridePanel.classList.add("show", "border", "ride");
        this.ridePanel.id = this.id;
        // Info
        const title = document.createElement("h2");
        title.classList.add("title");
        title.textContent = this.name;
        this.ridePanel.appendChild(title);
        this.ridePanel.appendChild(document.createElement("hr"));
        const controlPannel = document.createElement("div");
        controlPannel.classList.add("ride-pannel-info");
        const sales = document.createElement("p");
        sales.id = this.id + "-sales";
        sales.classList.add("text", "ride-pannel-info-item");
        sales.textContent = "Sales: " + this.rideSales.toString();
        controlPannel.appendChild(sales);
        const runCost = document.createElement("p");
        runCost.id = this.id + "-rcost";
        runCost.classList.add("text", "ride-pannel-info-item");
        runCost.textContent = "Running Cost: " + this.rideRunningCost.toString();
        controlPannel.appendChild(runCost);
        const rideCapacity = document.createElement("p");
        rideCapacity.id = this.id + "-cap";
        rideCapacity.classList.add("text", "ride-pannel-info-item");
        rideCapacity.textContent = "Capacity: " + this.rideCapacity.toString();
        controlPannel.appendChild(rideCapacity);
        const rideMaxCapacity = document.createElement("p");
        rideMaxCapacity.id = this.id + "-ride-max-cap";
        rideMaxCapacity.classList.add("text", "ride-pannel-info-item");
        rideMaxCapacity.textContent = "Max Capacity: " + this.rideMaxCapacity.toString();
        controlPannel.appendChild(rideMaxCapacity);
        const rideQueue = document.createElement("p");
        rideQueue.id = this.id + "-queue";
        rideQueue.classList.add("text", "ride-pannel-info-item");
        rideQueue.textContent = "Queue: " + this.rideQueue.toString();
        controlPannel.appendChild(rideQueue);
        const sat = document.createElement("p");
        sat.id = this.id + "-sat";
        sat.classList.add("text", "ride-pannel-info-item");
        sat.textContent = "Satisfaction: " + this.rideSatisfaction.toString();
        controlPannel.appendChild(sat);
        const saleButton = document.createElement("button");
        saleButton.classList.add("button", "text", "ride-pannel-info-item");
        saleButton.id = this.id + "-sell-ticket";
        saleButton.textContent = (this.hasBought ? (this.isBroken ? "⚠️ Restore Ride: £" + this.rideMaintianceCost : "Sell Ticket: £" + this.rideTicketCost) : "Buy Ride: £" + this.rideCost);
        controlPannel.appendChild(saleButton);
        saleButton.addEventListener("mousedown", e => {
            if (e.button != 0)
                return;
            if (this.hasBought) {
                if (this.isBroken) {
                    if (money.getMoney() - this.rideCost >= 0) {
                        money.setMoney(-this.rideCost);
                        this.isBroken = false;
                        saleButton.textContent = (this.hasBought ? (this.isBroken ? "⚠️ Restore Ride: £" + this.rideMaintianceCost + " ⚠️" : "Sell Ticket: £" + this.rideTicketCost) : "Buy Ride: £" + this.rideCost);
                    }
                }
                else {
                    this.sellTicket();
                }
            }
            else {
                if (money.getMoney() - this.rideCost >= 0) {
                    money.setMoney(-this.rideCost);
                    this.hasBought = true;
                    saleButton.textContent = (this.hasBought ? (this.isBroken ? "⚠️ Restore Ride: £" + this.rideMaintianceCost + " ⚠️" : "Sell Ticket: £" + this.rideTicketCost) : "Buy Ride: £" + this.rideCost);
                }
            }
        });
        // Upgrades
        const autoTicketContainer = document.createElement("div");
        autoTicketContainer.classList.add("show", "ride-pannel-info");
        autoTicketContainer.id = this.id + "-upgrade-autoticket-container";
        const autoTickerInfo = document.createElement("p");
        autoTickerInfo.classList.add("text", "ride-pannel-info-item");
        autoTickerInfo.id = this.id + "-upgrade-autoticket-info";
        autoTickerInfo.textContent = "No. of Ticket Staff: " + this.u_AutoTicketQuantity;
        autoTicketContainer.appendChild(autoTickerInfo);
        const autoTickerButton = document.createElement("button");
        autoTickerButton.classList.add("button", "text", "ride-pannel-info-item");
        autoTickerButton.id = this.id + "-upgrade-autoticket-but";
        autoTickerButton.textContent = "Hire Ticket Staff: £" + this.u_AutoTicketCost.toString();
        autoTicketContainer.appendChild(autoTickerButton);
        autoTickerButton.addEventListener("mousedown", e => {
            if (e.button != 0)
                return;
            if (money.getMoney() - this.u_AutoTicketCost >= 0) {
                money.setMoney(-this.u_AutoTicketCost);
                this.u_AutoTicketQuantity++;
                this.rideRunningCost += this.u_AutoTicketRunningCost;
            }
        });
        controlPannel.appendChild(autoTicketContainer);
        // Don't add components to controlPannel beyond this point
        this.ridePanel.appendChild(controlPannel);
        return this.ridePanel;
    }
    updateElemets() {
        const sales = document.getElementById(this.id + "-sales");
        sales.textContent = "Sales: " + this.rideSales.toString();
        const rCost = document.getElementById(this.id + "-rcost");
        rCost.textContent = "Running Cost: £" + this.rideRunningCost.toString();
        const queue = document.getElementById(this.id + "-queue");
        queue.textContent = "Queue: " + this.rideQueue.toString();
        const sat = document.getElementById(this.id + "-sat");
        sat.textContent = "Satisfaction: " + this.rideSatisfaction.toString();
        const cap = document.getElementById(this.id + "-cap");
        cap.textContent = "Capacity: " + this.rideCapacity.toString();
        const maxCap = document.getElementById(this.id + "-ride-max-cap");
        maxCap.textContent = "Max Capacity: " + this.rideMaxCapacity.toString();
        const saleButton = document.getElementById(this.id + "-sell-ticket");
        if (this.isBroken) {
            const rideCon = document.getElementById(this.id);
            rideCon.classList.add("ride-down");
            saleButton.textContent = (this.hasBought ? (this.isBroken ? "⚠️ Restore Ride: £" + this.rideMaintianceCost + " ⚠️" : "Sell Ticket: £" + this.rideTicketCost) : "Buy Ride: £" + this.rideCost);
        }
        else {
            const rideCon = document.getElementById(this.id);
            rideCon.classList.remove("ride-down");
            saleButton.textContent = (this.hasBought ? (this.isBroken ? "⚠️ Restore Ride: £" + this.rideMaintianceCost + " ⚠️" : "Sell Ticket: £" + this.rideTicketCost) : "Buy Ride: £" + this.rideCost);
        }
        const autoTickerInfo = document.getElementById(this.id + "-upgrade-autoticket-info");
        autoTickerInfo.textContent = "No. of Ticket Staff: " + this.u_AutoTicketQuantity;
        const autoTickerButton = document.getElementById(this.id + "-upgrade-autoticket-but");
        autoTickerButton.textContent = "Hire Ticket Staff: £" + this.u_AutoTicketCost.toString();
        const autoTicketContainer = document.getElementById(this.id + "-upgrade-autoticket-container");
        if (this.rideSales >= this.#ur_AutoTicket) {
            autoTicketContainer.classList.replace("hide", "show");
        }
        else {
            autoTicketContainer.classList.add("show", "hide");
        }
    }
}
export default Ride;
