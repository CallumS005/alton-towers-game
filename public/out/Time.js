class Time {
    #m_DeltaTime;
    #m_FrameStartTime;
    #m_FrameEndTime;
    frameStart() {
        this.#m_FrameStartTime = Date.now();
    }
    frameEnd() {
        this.#m_FrameEndTime = Date.now();
    }
    getDelta() {
        return this.#m_DeltaTime = (this.#m_FrameStartTime - this.#m_FrameEndTime) / 1000;
    }
    getTime() {
        return Date.now();
    }
}
const time = new Time();
export { time };
